# Ressources

## Git

- [Vidéos : "concepts clés de Git"](https://www.youtube.com/playlist?list=PLPoAfTkDs_JbMPY-BXzud0aMhKKhcstRc) 
- [Visualisation interactive des zones](http://ndpsoftware.com/git-cheatsheet.html)
- Visualisation interactive de l'action des commandes :
    - [Explain Git with D3](https://onlywei.github.io/explain-git-with-d3/)
    - [Learning Git branching](https://learngitbranching.js.org/)

### Documentation

- [Le « Git Book » officiel](https://git-scm.com/book/fr/v2/)
- [Les commandes classées par aspect](https://git-scm.com/docs/)
- Les manuels Git :
    - [git help glossary](https://git-scm.com/docs/gitglossary)
    - [git help cli](https://git-scm.com/docs/gitcli)
    - [git help tutorial](https://git-scm.com/docs/gittutorial)
    - [git help tutorial-2](https://git-scm.com/docs/gittutorial-2)
    - [git help core-tutorial](https://git-scm.com/docs/gitcore-tutorial)

### Articles / Tutorials
- [Git log pour analyser l’historique des commits](https://delicious-insights.com/fr/articles/git-log/)
- [Bien utiliser Git merge et rebase](https://delicious-insights.com/fr/articles/bien-utiliser-git-merge-et-rebase/)
- [Git reset : rien ne se perd, tout se transforme](https://delicious-insights.com/fr/articles/git-reset/)
- [Workflow Git : objectifs et principes généraux](https://delicious-insights.com/fr/articles/git-workflows-generality/)

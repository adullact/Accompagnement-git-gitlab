# Pré-requis

## Logistique

Voici les éléments logistiques nécessaires pour le déroulement de la journée.

1. Une salle avec tableau blanc, un vidéo-projecteur, prises électriques (1/personne minimum)
1. Chaque participant (sauf profil décideur) amène son poste de travail ; ceci permet de travailler "pour de vrai", et identifier en direct d'éventuels problèmes invisibles autrement
1. Chaque participant (sauf profil décideur) doit avoir un accès à internet vérifiant les conditions suivantes :
    * Port 22 ouvert vers `gitlab.adullact.net` pour faire du `git+ssh`
    * Ports 80 et 443 ouverts vers l'internet
1. Si la 4G ne passait pas ou mal dans la salle : un accès internet pour l'intervenant Adullact, avec les caractéristiques ci-dessus.
1. Les prénom, nom et courriel des participants afin de leur communiquer les préparatifs techniques.

## Questions

Afin de faire connaissance, vous serait-il possible de répondre succinctement à ces questions ?

* Décrivez brièvement votre poste

À destination des informaticiens :

* Quel éditeur de texte ou environnement de développement utilisez-vous ?
* Quel système d'exploitation (et sa version) utilisez-vous ?
* Comment vous servez-vous de git : en ligne de commande ? avec un outil tiers ? (si oui lequel ?)

## Préparatifs techniques (à destination des informaticiens)

Création de compte :

1. Se [créer un compte sur Adullact.net](https://adullact.net/account/register.php).
2. Confirmer la création du compte en cliquant sur le lien reçu par courriel et en saisissant identifiant et mot de passe.
3. Se connecter à nouveau sur adullact.net. (La confirmation de création de compte donne l'impression d'être connecté, il n'en est rien :) Il est bel et bien nécessaire de bien se reconnecter après avoir confirmé son compte.)
4. Se connecter sur [gitlab.adullact.net](http://gitlab.adullact.net/) avec les mêmes identifiants
5. Confirmer son adresse électronique
6. Bienvenue !

Mise en place de la "chaîne Git" et vérification sur son poste de travail Windows :

* [Télécharger Git](https://git-scm.com/download/win) et l'installer
* Créer une clé SSH avec Git Bash : `ssh-keygen -t rsa -b 4096` (conserver les chemins proposés par défaut)
* Se connecter sur [gitlab.adullact.net](http://gitlab.adullact.net/)
* Suivre les instruction pour ajouter sa clé **publique** SSH (la clé étant créé, il n'y a plus qu'à copier la partie publique à l'emplacement dédié dans le Gitlab)
* Définir son identité : `git config --global user.name "Prénom NOM"`
* Définir son courriel : `git config --global user.email "moi@moncourriel.fr"`

Pour vérifier que tout est correct, merci de :

1. Lancer depuis le *Git Bash* la commande `git clone git@gitlab.adullact.net:adullact/Accompagnement-git-gitlab.git`,
1. **D'envoyer le résultat par courriel à Matthieu FAURE**

## Proxy

Pour configurer un proxy pour git il convient de taper :

```
git config --global http.proxy http://proxyuser:proxypwd@proxy.server.com:proxyport
```

Exemple : `git config --global http.proxy http://mfaure:motdepasse@proxy.adullact.org:8080`